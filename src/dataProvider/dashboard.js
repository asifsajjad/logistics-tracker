export class dashboardDataProvider {
    constructor() {
    }

        
    getData = ({ ACTIVE_USER }) => {
        return new Promise((res, rej) => { 
            res({
                transportStart: [
                    { name: "Spitfire", driver: "Ram", destination: "Kolkata" },
                ],
                transportEnd: [
                    { name: "SpiderMonkey", driver: "Shravan", destination: "Ranchi" },
                ],
                onTimeData: 112,
                earlyData: 5,
                delayData: 2,
                totalDistanceData: 10230,
                totalGoodsData: 56390,
                totalRevenueData: 12121,
                totalMaintenanceData: 2345,
                breakDownCostData: 4500,
                totalAmountReceivedData: 9900,
                totalAmountPendingData: 12121 - 9900,
            })
        })
        };
        
}
