import React, { Component } from 'react'
import { dashboardDataProvider } from "../dataProvider/dashboard";
import _ from "lodash";
import { Routes, Route, Outlet } from "react-router-dom";
import SideNav from '../components/SideNav';
import Transit from '../components/Transit';
import MainDashboard from '../components/MainDashboard';
import Journey from '../components/Journey';
import Truck from '../components/Truck';
import Trailer from '../components/Trailer';
import Driver from '../components/Driver';
import CreateModal from '../components/CreateModal';


export class Dashboard extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
      transportStart: null,
      transportEnd: null,
      onTimeData: null,
      earlyData: null,
      delayData: null,
      totalDistanceData: null,
      totalGoodsData: null,
      totalRevenueData: null,
      totalMaintenanceData: null,
      breakDownCostData: null,
      totalAmountReceivedData: null,
      totalAmountPendingData: null,
      selectedTab:""
    }
    this.selectTab = this.selectTab.bind(this);
  }

  componentDidMount() {
    const dashboard = new dashboardDataProvider();
    dashboard.getData({ ACTIVE_USER: this.props.ACTIVE_USER }).then((data) => {
      if (_.isEmpty(data)) {
        throw new Error(" OOPS! Couldn't fetch the dashboard data. Please refresh the page and try again");
      }

      const {
        transportStart = [],
        transportEnd = [],
        onTimeData = 0,
        earlyData = 0,
        delayData = 0,
        totalDistanceData = 0,
        totalGoodsData = 0,
        totalRevenueData = 0,
        totalMaintenanceData = 0,
        breakDownCostData = 0,
        totalAmountReceivedData = 0,
        totalAmountPendingData = 0
      } = data;

      const apiData = {
        transportStart,
        transportEnd,
        onTimeData,
        earlyData,
        delayData,
        totalDistanceData,
        totalGoodsData,
        totalRevenueData,
        totalMaintenanceData,
        breakDownCostData,
        totalAmountReceivedData,
        totalAmountPendingData
      };
      
      this.setState(apiData);
      console.log(this.state);
      
    }).catch(console.log);
  }

  selectTab =(tabName) => {
    this.setState({...this.state, selectedTab: tabName})
  }

  render() {
    return (
      <>
        <SideNav selectATab={this.selectTab} />
        {(this.state.selectedTab === "") ? <MainDashboard /> : null}
        {(this.state.selectedTab === "transit") ? <Transit /> : null}
        {(this.state.selectedTab === "journey") ? <Journey /> : null}
        {(this.state.selectedTab === "truck") ? <Truck /> : null}
        {(this.state.selectedTab === "trailer") ? <Trailer /> : null}
        {(this.state.selectedTab === "driver") ? <Driver renderCreateDialog={this.selectTab} /> : null}
        {(this.state.selectedTab === "createDriver") ? <CreateModal formName={this.state.selectedTab} /> : null}
        
</>
    )
  }
}

export default Dashboard

// function Dashboard({ACTIVE_USER}) {

//   const initialData = {
//     transportStart: null,
//     transportEnd: null,
//     onTimeData: null,
//     earlyData: null,
//     delayData: null,
//     totalDistanceData: null,
//     totalGoodsData: null,
//     totalRevenueData: null,
//     totalMaintenanceData: null,
//     breakDownCostData: null,
//     totalAmountReceivedData: null,
//     totalAmountPendingData: null
//   }

//   const fetchDashboardData = () => {

//     const dashboardDataProvider = apiDataProvider.dashboardDataProvider();
//     dashboardDataProvider.getData({ ACTIVE_USER }).then((data) => {
//       if (_.isEmpty(data)) {
//         throw new Error(" OOPS! Couldn't fetch the dashboard data. Please refresh the page and try again");
//       }

//       const {
//         transportStart = [],
//         transportEnd = [],
//         onTimeData = 0,
//         earlyData = 0,
//         delayData = 0,
//         totalDistanceData = 0,
//         totalGoodsData = 0,
//         totalRevenueData = 0,
//         totalMaintenanceData = 0,
//         breakDownCostData = 0,
//         totalAmountReceivedData = 0,
//         totalAmountPendingData = 0
//       } = data;

//       const apiData = {
//         transportStart,
//         transportEnd,
//         onTimeData,
//         earlyData,
//         delayData,
//         totalDistanceData,
//         totalGoodsData,
//         totalRevenueData,
//         totalMaintenanceData,
//         breakDownCostData,
//         totalAmountReceivedData,
//         totalAmountPendingData
//       };

//       setData(apiData);
//     }).catch(console.log);

//   }

//   const [data, setData] = useState(initialData);

//   fetchDashboardData();

//   return (
//     <div>Dashboard</div>
//   )
// }

// export default Dashboard