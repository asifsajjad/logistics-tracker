import './App.css';
import Cookies from 'js-cookie';
import axios from "axios";

import React, { Component } from 'react';

import Routers from './routes'
import Header from './components/Header';

export class App extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
      validated : false,
      ACTIVE_USER : "",
    }
  }

  // componentDidMount() {

  //   const AuthToken = Cookies.get("ACTIVE_USER");
  //   if (!AuthToken) {
  //     window.location.href = '/login';
      
  //   } else {
  //     this.setState({ ACTIVE_USER: "LOCAL" });
  //     Cookies.set("ACTIVE_USER", "LOCAL");
  //     return;
  //     axios.get("http://localhost:3500/authenticate/validateToken").then((response) => {
  //       if (response.data === "valid") {
  //         this.setState({ ACTIVE_USER : AuthToken });
  //       } else {
  //         window.location.href = '/login';
  //       }
  //     }, (error) => {
  //       console.log("Token validation was interrupted by some error!");
  //     })
  //   }
    
  // }
  render() {
    return (
      <>
        <Header />
        <Routers ACTIVE_USER={this.state.ACTIVE_USER}/>
      </>
    )
  }
}

export default App;


