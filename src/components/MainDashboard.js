import React from "react";

function MainDashboard() {
  return (
    <>
      <main id="main" className="main">
        <div className="pagetitle">
          <h1>Dashboard</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="index.html">Home</a>
              </li>
              <li className="breadcrumb-item active">Dashboard</li>
            </ol>
          </nav>
        </div>

        <section className="section dashboard">
          <div className="row">
            <div className="col-lg-8">
              <div className="row">
                <div className="col-xxl-4 col-md-6">
                  <div className="card info-card sales-card">
                    <div className="card-body">
                      <h5 className="card-title">
                        Starting <span>| Today</span>
                      </h5>

                      <div className="d-flex align-items-center">
                        <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
                          <i className="bi bi-truck"></i>
                        </div>
                        <div className="ps-3">
                          <h6>14</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-xxl-4 col-md-6">
                  <div className="card info-card revenue-card">
                    <div className="card-body">
                      <h5 className="card-title">
                        Revenue <span>| This Month</span>
                      </h5>

                      <div className="d-flex align-items-center">
                        <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
                          <i className="bi bi-currency-dollar"></i>
                        </div>
                        <div className="ps-3">
                          <h6>$3,264</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-xxl-4 col-xl-12">
                  <div className="card info-card customers-card">
                    <div className="card-body">
                      <h5 className="card-title">
                        Customers <span>| This Month</span>
                      </h5>

                      <div className="d-flex align-items-center">
                        <div className="card-icon rounded-circle d-flex align-items-center justify-content-center">
                          <i className="bi bi-people"></i>
                        </div>
                        <div className="ps-3">
                          <h6>124</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-12">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">
                        Track Record <span>| This month</span>
                      </h5>
                      <div className="d-flex justify-content-start gap-4">
                        <h1>
                          <i class="bi bi-graph-up-arrow"></i>
                        </h1>
                        <div className="ps-2">
                          <h4>
                            On Time: 30 <br />
                            Delay: 02 <br />
                            Early:05
                          </h4>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-12">
                    <div className="card recent-sales overflow-auto">
                      <div className="card-body">
                        <h5 className="card-title">
                          Recent Transits <span>| Overview</span>
                        </h5>

                        <table className="table table-borderless datatable">
                          <thead>
                            <tr>
                              <th scope="col">Id</th>
                              <th scope="col">Name</th>
                              <th scope="col">Driver</th>
                              <th scope="col">From</th>
                              <th scope="col">To</th>
                              <th scope="col">Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">
                                <a href="#">134</a>
                              </th>
                              <td>Jaipur odyssey</td>
                              <td>
                                <a href="#" className="text-primary">
                                  Ryan Reynolds
                                </a>
                              </td>
                              <td>Delhi</td>
                              <td>Jaipur</td>
                              <td>
                                <span className="badge bg-success">
                                  Loading
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">
                                <a href="#">134</a>
                              </th>
                              <td>Jaipur odyssey</td>
                              <td>
                                <a href="#" className="text-primary">
                                  Ryan Reynolds
                                </a>
                              </td>
                              <td>Delhi</td>
                              <td>Jaipur</td>
                              <td>
                                <span className="badge bg-warning">
                                  Waiting
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">
                                <a href="#">134</a>
                              </th>
                              <td>Jaipur odyssey</td>
                              <td>
                                <a href="#" className="text-primary">
                                  Ryan Reynolds
                                </a>
                              </td>
                              <td>Delhi</td>
                              <td>Jaipur</td>
                              <td>
                                <span className="badge bg-success">
                                  Loading
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <th scope="row">
                                <a href="#">134</a>
                              </th>
                              <td>Jaipur odyssey</td>
                              <td>
                                <a href="#" className="text-primary">
                                  Ryan Reynolds
                                </a>
                              </td>
                              <td>Delhi</td>
                              <td>Jaipur</td>
                              <td>
                                <span className="badge bg-danger">Delay</span>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}

export default MainDashboard;
