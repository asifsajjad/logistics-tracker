import React from 'react'

function Truck() {
  return (
    <>
    <main id="main" className="main">
            <div className="pagetitle">
              <h1>Dashboard</h1>
              <nav>
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <a href="/">Home</a>
                  </li>
                  <li className="breadcrumb-item active">truck</li>
                </ol>
              </nav>
            </div>
    
            <section className="section dashboard">
              <div className="row">
                <div className="col-lg-8">
                              <div>
                              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Truck details</h5>
    
                 
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Brand</th>
                        <th scope="col">Model</th>
                        <th scope="col">Year</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Truck 3425</td>
                        <td>Volvo</td>
                        <td>G308</td>
                              <td>2019</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Truck 3425</td>
                        <td>Volvo</td>
                        <td>G308</td>
                              <td>2019</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Truck 3425</td>
                        <td>Volvo</td>
                        <td>G308</td>
                              <td>2019</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Truck 3425</td>
                        <td>Volvo</td>
                        <td>G308</td>
                              <td>2019</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Truck 3425</td>
                        <td>Volvo</td>
                        <td>G308</td>
                              <td>2019</td>
                      </tr>
                    </tbody>
                  </table>
                  
    
                </div>
              </div>
                  </div>
                </div>
              </div>
            </section>
            <button type="button" class="btn btn-outline-primary btn-lg">
            <h1><i class="bi bi-plus-square"></i></h1>
              <h2>Create Truck</h2>
            </button>
          </main>
        </>
  )
}

export default Truck