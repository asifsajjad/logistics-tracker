import React from "react";

function SideNav({selectATab}) {
  return (
    <>
      <aside id="sidebar" className="sidebar">
        <ul className="sidebar-nav" id="sidebar-nav">
          <li className="nav-item">
            <a className="nav-link pointer" onClick={()=>selectATab("transit")}>
              <i className="bi bi-sign-intersection-t"></i>
              <span>Transit</span>
            </a>
          </li>

          <li className="nav-item">
                      <a className="nav-link " onClick={() => selectATab("journey")}>
              <i className="bi bi-signpost-split"></i>
              <span>Journeys</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link " onClick={() => selectATab("truck")}>
              <i className="bi bi-truck-flatbed"></i>
              <span>Trucks</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link " onClick={() => selectATab("trailer")}>
              <i className="bi bi-train-front"></i>
              <span>Trailers</span>
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link " onClick={() => selectATab("driver")}>
              <i className="bi bi-person"></i>
              <span>Drivers</span>
            </a>
          </li>

          <li className="nav-heading">Miscellaneous</li>

          <li className="nav-item">
            <a className="nav-link collapsed" onClick={() => selectATab("track")}>
              <i className="bi bi-geo-alt"></i>
              <span>Track vehicle</span>
            </a>
          </li>
        </ul>
      </aside>
    </>
  );
}

export default SideNav;
