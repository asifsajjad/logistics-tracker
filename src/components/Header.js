import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import axios from "axios";

function Header({ ACTIVE_USER }) {
  const initialData = {
    thumbnail: "",
    userName: "",
  };
  const [data, setData] = useState(initialData);
  useEffect(() => {
    setData({ ...data, userName: "Asif" });
  }, [data.userName]);

  const signOut = () => {
    console.log("Signout called!");
    Cookies.remove("ACTIVE_USER");
    window.location.href = "/";
  };

  const fetchUserDetails = (ACTIVE_USER) => {
    console.log("User fetch called...");
    axios.get("http://localhost:4500/api/get-user").then((response) => {
      if (!response.data) {
        throw new Error("OOPS! The user data couldn't be fetched.");
      }
      const { name = "", thumbnail = "" } = response.data;
      setData({ ...data, userName: name, thumbnail });
    });
  };

  return (
    <>
 <header id="header" className="header fixed-top d-flex align-items-center">

<div className="d-flex align-items-center justify-content-between">
  <a href="/" className="logo d-flex align-items-center">
    <img src="assets/img/logo.png" alt="" />
    <span className="d-none d-lg-block">Company name</span>
  </a>
  <i className="bi bi-list toggle-sidebar-btn"></i>
</div>

<div className="d-flex align-items-center justify-content-between">
  <a href="/" className="d-flex align-items-center p-2">
    <span className="d-lg-block">Dashboard</span>
        </a>
  <a href="/accounting" className="d-flex align-items-center p-2">
    <span className="d-lg-block">Accounting</span>
        </a>
</div>

<nav className="header-nav ms-auto">
  <ul className="d-flex align-items-center">


    <li className="nav-item dropdown pe-3">

      <a className="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
        <img src="assets/img/profile-img.jpg" alt="Profile" className="rounded-circle" />
        <span className="d-none d-md-block dropdown-toggle ps-2">Asif</span>
      </a>

      <ul className="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
        <li className="dropdown-header">
          <h6>Asif Sajjad</h6>
          <span>Admin</span>
        </li>
        <li>
          <hr className="dropdown-divider" />
        </li>

        <li>
          <a className="dropdown-item d-flex align-items-center" href="/settings/profile">
            <i className="bi bi-person"></i>
            <span>My Profile</span>
          </a>
        </li>
        <li>
          <hr className="dropdown-divider" />
        </li>

        <li>
          <a className="dropdown-item d-flex align-items-center" href="/settings/main">
            <i className="bi bi-gear"></i>
            <span>Settings</span>
          </a>
        </li>
        <li>
          <hr className="dropdown-divider" />
        </li>

        <li>
          <a className="dropdown-item d-flex align-items-center" href="/settings/data">
            <i className="bi bi-check2-square"></i>
            <span>My Data</span>
          </a>
        </li>
        <li>
          <hr className="dropdown-divider" />
              </li>
              
              <li>
          <a className="dropdown-item d-flex align-items-center" href="/settings/integrations">
            <i className="bi bi-gear-wide-connected"></i>
            <span>Integrations</span>
          </a>
        </li>
        <li>
          <hr className="dropdown-divider" />
        </li>

        <li>
          <a className="dropdown-item d-flex align-items-center" href="#">
            <i className="bi bi-box-arrow-right"></i>
            <span>Sign Out</span>
          </a>
        </li>

      </ul>
    </li>

  </ul>
</nav>

</header>
    </>
  );
}

export default Header;
