import React from 'react'

function Driver({ACTIVE_USER, renderCreateDialog}) {
  return (
    <>
    <main id="main" className="main">
            <div className="pagetitle">
              <h1>Dashboard</h1>
              <nav>
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <a href="/">Home</a>
                  </li>
                  <li className="breadcrumb-item active">driver</li>
                </ol>
              </nav>
            </div>
    
            <section className="section dashboard">
              <div className="row">
                <div className="col-lg-8">
                              <div>
                              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Driver details</h5>
    
                 
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Driving type</th>
                        <th scope="col">Verified</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Richard P</td>
                        <td>22,VV Puram, Bengaluru</td>
                        <td>Heavy truck</td>
                              <td>Yes</td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>Daniel S</td>
                        <td>121,Indira Nagar, Bengaluru</td>
                        <td>Light Truck</td>
                              <td>Yes</td>
                      </tr>
                    </tbody>
                  </table>
                  
    
                </div>
              </div>
                  </div>
                </div>
              </div>
            </section>
            <button type="button" class="btn btn-outline-primary btn-lg" onClick={() => renderCreateDialog("createDriver")}>
            <h1><i class="bi bi-plus-square"></i></h1>
              <h2>Create Driver</h2>
            </button>
          </main>
        </>
  )
}

export default Driver