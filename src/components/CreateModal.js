import React from "react";

function CreateModal({ ACTIVE_USER, formName }) {
  return (
    <>
      <main id="main" className="main">
        <div className="pagetitle">
          <h1>Create Mode</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item active">{formName}</li>
            </ol>
          </nav>
        </div>

        <section className="section dashboard">
          <div className="row">
            <div className="col-lg-8">
              <div>
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">{formName}</h5>
                    <form class="row g-3">
                      <div class="col-md-12">
                        <label for="inputName5" class="form-label">
                          Name
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          id="inputName5"
                        />
                      </div>
                      {formName === "createTruck" ? (
                        <div class="col-md-12">
                          {" "}
                          <label for="inputRTO" class="form-label">
                            RTO Number
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="inputRTO"
                          />
                        </div>
                      ) : null}
                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-6">
                          <label for="inputEmail5" class="form-label">
                            Email
                          </label>
                          <input
                            type="email"
                            class="form-control"
                            id="inputEmail5"
                          />
                        </div>
                      ) : null}
                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-6">
                          <label for="inputPhone" class="form-label">
                            Phone
                          </label>
                          <input
                            type="number"
                            class="form-control"
                            id="inputPhone"
                          />
                        </div>
                      ) : null}
                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-4">
                          <label for="dLicense" class="form-label">
                            Driving License
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="dLicense"
                          />
                        </div>
                      ) : null}
                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-4">
                          <label for="experience" class="form-label">
                            Experience
                          </label>
                          <input
                            type="number"
                            class="form-control"
                            id="experience"
                          />
                        </div>
                      ) : null}
                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-4">
                          <label for="gender" class="form-label">
                            Gender
                          </label>
                          <select id="gender" class="form-select">
                            <option selected>Choose...</option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Others</option>
                          </select>
                        </div>
                      ) : null}
                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-12">
                          <label for="inputAddress5" class="form-label">
                            Address
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="inputAddres5s"
                            placeholder="1234 Main St"
                          />
                        </div>
                      ) : null}

                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-6">
                          <label for="inputCity" class="form-label">
                            City
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="inputCity"
                          />
                        </div>
                      ) : null}
                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-4">
                          <label for="inputState" class="form-label">
                            State
                          </label>
                          <select id="inputState" class="form-select">
                            <option selected>Choose...</option>
                            <option>...</option>
                          </select>
                        </div>
                      ) : null}

                      {formName === "createDriver" ||
                      formName === "createCustomer" ? (
                        <div class="col-md-2">
                          <label for="inputZip" class="form-label">
                            Zip
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="inputZip"
                          />
                        </div>
                      ) : null}
                      <div class="col-12">
                        <div class="form-check">
                          <input
                            class="form-check-input"
                            type="checkbox"
                            id="gridCheck"
                          />
                          <label class="form-check-label" for="gridCheck">
                            I have verified the details entered in the form.
                          </label>
                        </div>
                      </div>
                      <div class="text-center d-flex gap-4">
                        <button type="submit" class="btn btn-primary">
                          Submit
                        </button>
                        <button type="reset" class="btn btn-secondary">
                          Reset
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}

export default CreateModal;
