import Cookies from 'js-cookie';

const RouteGuard = ({children}) => {

    function hasJWT() {
        let flag = false;
        const AuthToken = Cookies.get("ACTIVE_USER");

        //check user has JWT token
        AuthToken ? flag=true : flag=false
        
        return flag
    }
    if (hasJWT()) {
        return children;
    }
    
    
    window.location.href= "/login";

};

export default RouteGuard;