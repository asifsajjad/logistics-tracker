import React from 'react'

function Trailer() {
  return (
    <>
    <main id="main" className="main">
            <div className="pagetitle">
              <h1>Dashboard</h1>
              <nav>
                <ol className="breadcrumb">
                  <li className="breadcrumb-item">
                    <a href="/">Home</a>
                  </li>
                  <li className="breadcrumb-item active">trailer</li>
                </ol>
              </nav>
            </div>
    
            <section className="section dashboard">
              <div className="row">
                <div className="col-lg-8">
                              <div>
                              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Trailer details</h5>
    
                 
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Category</th>
                        <th scope="col">Load Capacity(kg)</th>
                        <th scope="col">Volume Capacity(ltr)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Trailer 1190</td>
                        <td>Medium</td>
                        <td>2230</td>
                              <td>15000</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Trailer 1190</td>
                        <td>Medium</td>
                        <td>2230</td>
                              <td>15000</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Trailer 1190</td>
                        <td>Medium</td>
                        <td>2230</td>
                              <td>15000</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Trailer 1190</td>
                        <td>Medium</td>
                        <td>2230</td>
                              <td>15000</td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Trailer 1190</td>
                        <td>Medium</td>
                        <td>2230</td>
                              <td>15000</td>
                      </tr>
                    </tbody>
                  </table>
                  
    
                </div>
              </div>
                  </div>
                </div>
              </div>
            </section>
            <button type="button" class="btn btn-outline-primary btn-lg">
            <h1><i class="bi bi-plus-square"></i></h1>
              <h2>Create Trailer</h2>
            </button>
          </main>
        </>
  )
}

export default Trailer