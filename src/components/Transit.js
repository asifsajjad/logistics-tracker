import React from "react";

function Transit({ACTIVE_USER}) {
  return (
    <>
      <main id="main" className="main">
        <div className="pagetitle">
          <h1>Dashboard</h1>
          <nav>
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item active">transit</li>
            </ol>
          </nav>
        </div>

        <section className="section dashboard">
          <div className="row">
            <div className="col-lg-8">
                          <div>
                          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Transit details</h5>

             
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Route</th>
                    <th scope="col">Start Date</th>
                          <th scope="col">End Date</th>
                          <th scope="col">Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Delhi Line</td>
                    <td>Delhi to Chandigarh</td>
                    <td>2023-02-19</td>
                          <td>2023-02-25</td>
                          <td>On time</td>
                  </tr>
                  <tr>
                    <th scope="row">1</th>
                    <td>Delhi Line</td>
                    <td>Delhi to Chandigarh</td>
                    <td>2023-02-19</td>
                          <td>2023-02-25</td>
                          <td>On time</td>
                  </tr>
                  <tr>
                    <th scope="row">1</th>
                    <td>Delhi Line</td>
                    <td>Delhi to Chandigarh</td>
                    <td>2023-02-19</td>
                          <td>2023-02-25</td>
                          <td>On time</td>
                  </tr>
                  <tr>
                    <th scope="row">1</th>
                    <td>Delhi Line</td>
                    <td>Delhi to Chandigarh</td>
                    <td>2023-02-19</td>
                          <td>2023-02-25</td>
                          <td>On time</td>
                  </tr>
                  <tr>
                    <th scope="row">1</th>
                    <td>Delhi Line</td>
                    <td>Delhi to Chandigarh</td>
                    <td>2023-02-19</td>
                          <td>2023-02-25</td>
                          <td>On time</td>
                  </tr>
                </tbody>
              </table>
              

            </div>
          </div>
              </div>
            </div>
          </div>
        </section>
        <button type="button" class="btn btn-outline-primary btn-lg">
        <h1><i class="bi bi-plus-square"></i></h1>
          <h2>Create Transit</h2>
        </button>
      </main>
    </>
  );
}

export default Transit;
