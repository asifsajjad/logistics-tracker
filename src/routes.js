import React from "react";
import { Routes, Route } from "react-router-dom";
import RouteGuard from "./components/RouteGuard";

//pages
import Analytics from "./pages/Analytics";
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import Profile from "./pages/Profile";
import Accounting from "./pages/Accounting";
import Settings from "./pages/Settings";
import Integrations from "./pages/Integrations";
import Dashboard from "./pages/Dashboard";

function MyRoutes({ACTIVE_USER}) {
  return (
    <>
      <Routes>
        <Route
          path="/analytics"
          element={<Analytics />}
        />
        <Route path="/login" element={ <Login />} />
        <Route path="/signup" element={<Signup />} />
        <Route
          path="/profile/:id"
          element={<Profile />}
        />
        <Route
          path="/settings"
          element={<Settings />}
        />
        <Route
          path="/accounting"
          element={<Accounting />}
        />
        <Route
          path="/integrations"
                  element={
                      <RouteGuard >
                      <Integrations />
                          </RouteGuard>
                        }
        />
        <Route path="/" element={< Dashboard />} />
      </Routes>
    </>
  );
}

export default MyRoutes;
